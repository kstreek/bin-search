# round two (here we go)

CPPFLAGS=-std=c++14 -Wall 
CC=g++
C2FLGS=--success --use-colour yes
TEST1=[test1]
TEST2=[test2]
TEST3=[test3]

all:
	$(CC) $(CPPFLAGS) -o bin/search.out src/main.cpp  
	

run:
	bin/search.out

runt:
	@bin/tests.out $(C2FLGS) || true #force make to be silent
runt1: 
	@bin/tests.out $(TEST1) $(C2FLGS) || true

runt2:
	@bin/tests.out $(TEST2) $(C2FLGS) || true

runt3: 
	@bin/tests.out $(TEST3) $(C2FLGS) || true


tests: bin/tests.o
	@printf "\033[33mCompiling Tests...\n\033[0m"	
	$(CC) $(CPPFLAGS) -o bin/tests.out bin/tests.o tests/tests.cpp 

bin/tests.o: tests/tests.cpp tests/tests_main.cpp src/search.hpp 
	@printf "\033[36mCompiling Test Driver...(be patient)...\n\033[0m"	
	$(CC) $(CPPFLAGS)  -c tests/tests_main.cpp -o bin/tests.o

clean:
	@printf "\033[31mRemoving objects (and temporary files)\033[0m\n"
	@rm -rf bin/*.o*
